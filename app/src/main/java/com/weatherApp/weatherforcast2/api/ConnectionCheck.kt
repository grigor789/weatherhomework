package com.weatherApp.weatherforcast2.api

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import androidx.lifecycle.LiveData

class ConnectionCheck(private val context: Context) : LiveData<Boolean>() {

    private var connectivityManager: ConnectivityManager =
        context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    private fun checkConnection(){
       var connected=false
        val allNetworks=connectivityManager.activeNetwork
        if(allNetworks!=null){

            val networkCapabilities:NetworkCapabilities?=connectivityManager.getNetworkCapabilities(allNetworks)
            if(networkCapabilities!=null){
                connected = (networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI))or
                        networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) or
                        networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)
            }
        }

        postValue(connected)
    }


    private val  networkReceiver=object :BroadcastReceiver(){
        override fun onReceive(context: Context?, intent: Intent?) {
           checkConnection()
        }

    }

    override fun onActive() {
        super.onActive()
        checkConnection()
        context.registerReceiver(networkReceiver,IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
    }

    override fun onInactive() {
        super.onInactive()
        context.unregisterReceiver(networkReceiver)
    }
}