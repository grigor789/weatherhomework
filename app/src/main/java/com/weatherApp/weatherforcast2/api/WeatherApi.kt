package com.weatherApp.weatherforcast2.api


import com.weatherApp.weatherforcast2.model.currnet.WeatherModel
import com.weatherApp.weatherforcast2.model.daily.DailyWeather
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherApi {

    @GET("weather")
    suspend fun getCurrentWeather(@Query("appid")key:String,@Query("q")city:String, @Query("units") unit:String  )
    :Response<WeatherModel>

    @GET("onecall")
    suspend fun getDaily7days(@Query("appid")key:String,@Query("lat") lat:String,@Query("lon")lon:String,@Query("units") unit:String,
    @Query("exclude")exclude:String)
    :Response<DailyWeather>



}