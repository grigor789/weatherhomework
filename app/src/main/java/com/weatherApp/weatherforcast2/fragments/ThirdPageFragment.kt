package com.weatherApp.weatherforcast2.fragments

import CountriesAndCitiesGson
import android.app.AlertDialog

import android.os.Bundle
import android.text.InputType
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.databinding.DataBindingUtil

import androidx.lifecycle.ViewModelProvider
import com.example.weatherforcast2.R


import com.example.weatherforcast2.databinding.FragmentThirdPageBinding

import com.google.gson.Gson
import com.weatherApp.weatherforcast2.viewmodel.NetworkViewModel
import okio.Utf8
import java.io.BufferedReader
import java.io.File
import java.io.FileInputStream
import java.io.InputStreamReader
import java.lang.StringBuilder

import java.util.*
import kotlin.collections.ArrayList


class ThirdPageFragment : Fragment() {

    private lateinit var binding: FragmentThirdPageBinding
    private lateinit var viewModel: NetworkViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_third_page, container, false)


        viewModel=ViewModelProvider(activity!!).get(NetworkViewModel::class.java)
        binding.searchBtnText.setOnClickListener { manualSearch(binding)}
        return binding.root
    }



    private fun manualSearch(binding: FragmentThirdPageBinding?) {
        binding!!.textInputLayout.editText!!.inputType =
            InputType.TYPE_CLASS_TEXT //or InputType.TYPE_TEXT_FLAG_CAP_SENTENCES
        if (!binding.textInputLayout.editText!!.text.isNullOrEmpty()) {
            viewModel.getSearchWeather(binding.textInputLayout.editText!!.text.toString())
            viewModel.check.observe(activity!!,
                { t -> if(t==200){activity!!.supportFragmentManager.beginTransaction().addToBackStack("a").replace(R.id.container,FourPageFragment()).commit()}else
                    binding.textInputLayout.error = " "
                })
    }
}

}
