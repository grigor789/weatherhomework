package com.weatherApp.weatherforcast2.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.weatherforcast2.R

class SearchFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        activity!!.supportFragmentManager.beginTransaction().add(R.id.container,ThirdPageFragment()).commit()
        return inflater.inflate(R.layout.fragment_search, container, false)
    }


}