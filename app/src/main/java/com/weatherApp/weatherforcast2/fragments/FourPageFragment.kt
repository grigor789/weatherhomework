package com.weatherApp.weatherforcast2.fragments

import android.annotation.SuppressLint
import android.graphics.Typeface
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.weatherforcast2.R

import com.example.weatherforcast2.databinding.FragmentPageFourBinding

import com.weatherApp.weatherforcast2.model.daily.Daily
import com.weatherApp.weatherforcast2.viewmodel.NetworkViewModel
import java.text.SimpleDateFormat


class FourPageFragment : Fragment() {
    private lateinit var viewModel: NetworkViewModel
    var dailyWeatherList:List<Daily>?=null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel = ViewModelProvider(activity!!)[NetworkViewModel::class.java]
        val binding: FragmentPageFourBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_page_four, container, false)
        init(binding)
        setSpinner(binding)
        setViews(binding)
        binding.appCompatButtonBack.setOnClickListener { activity!!.supportFragmentManager.beginTransaction().replace(R.id.container,ThirdPageFragment()).commit() }
        return binding.root
    }


    private fun init(binding: FragmentPageFourBinding) {
        val typeface = Typeface.createFromAsset(activity?.assets, "fonts/weather.ttf")
        binding.viewModel = viewModel
        binding.lifecycleOwner = activity

        binding.currentTemperature.typeface=typeface
    }

    @SuppressLint("SimpleDateFormat")
    private fun setSpinner(binding: FragmentPageFourBinding) {
      //  val time:Long=list[0].daily[position].dt.toLong()*1000
        //SimpleDateFormat("dd/MM/yyyy").format(time)

        val dayList=ArrayList<String>()
        viewModel.searchDailyWeatherLiveData.observe(activity!!,
            { t -> if(t!=null){dailyWeatherList=t.daily}

        if (dailyWeatherList!=null){
            for (dailyWeather in dailyWeatherList!!){
                val dayLong=dailyWeather.dt.toLong()*1000
                val day= SimpleDateFormat("dd/MM/yyyy").format(dayLong)
                dayList.add(day)
            }
        }
        val adapter = context?.let {
            ArrayAdapter(
                it,
                R.layout.spinneritem,
                dayList)
        }
        binding.spinnerDate.adapter=adapter

    })
    }
    private fun setViews(binding: FragmentPageFourBinding) {
        binding.spinnerDate.onItemSelectedListener=object :AdapterView.OnItemSelectedListener{
            @SuppressLint("SimpleDateFormat")
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) { if(dailyWeatherList!=null){
               val spinnerItem=binding.spinnerDate.getItemAtPosition(position).toString()
                for (daily in dailyWeatherList!!){
                    val dayLong=daily.dt.toLong()*1000
                    val day= SimpleDateFormat("dd/MM/yyyy").format(dayLong)
                    if (day==spinnerItem){
                        viewModel.searchDailyWeatherLiveData2.value=daily
                    }
                }

            }

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }
    }



}