package com.weatherApp.weatherforcast2.fragments


import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.graphics.Typeface
import android.location.Geocoder
import android.os.Bundle
import android.os.Looper
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.weatherforcast2.R
import com.example.weatherforcast2.databinding.FragmentFirstPageBinding

import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.weatherApp.weatherforcast2.model.latlon.LocationModel
import com.weatherApp.weatherforcast2.viewmodel.NetworkViewModel
import java.util.*


class FirstPageFragment : Fragment() {
    private lateinit var viewModel: NetworkViewModel


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding: FragmentFirstPageBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_first_page, container, false)
        viewModel = ViewModelProvider(activity!!)[NetworkViewModel::class.java]
        init(binding)

        location()
        binding.btnUpdate.setOnClickListener { location() }
        return binding.root
    }



    private fun init(binding: FragmentFirstPageBinding) {
        val typeface = Typeface.createFromAsset(activity?.assets, "fonts/weather.ttf")
        binding.viewModel = viewModel
        binding.lifecycleOwner = activity
        binding.weatherIcon.typeface = typeface
    }

    private fun location() {

        if (ContextCompat.checkSelfPermission(
                activity!!.applicationContext,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                1
            )
        } else getCurrentLocation()

    }

    @SuppressLint("MissingPermission")
    private fun getCurrentLocation() {
        val locationRequest = LocationRequest()
        locationRequest.interval = 10000
        locationRequest.fastestInterval = 3000
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        val mfusedLocationProviderClient =
            LocationServices.getFusedLocationProviderClient(activity!!)
        mfusedLocationProviderClient.requestLocationUpdates(
            locationRequest,
            object : LocationCallback() {
                override fun onLocationResult(locationResult: LocationResult?) {
                    super.onLocationResult(locationResult)
                    mfusedLocationProviderClient.removeLocationUpdates(this)
                    if (locationResult != null && locationResult.locations.size > 0) {
                        val latestLocationIndex = locationResult.locations.size - 1
                        val latitude = locationResult.locations.get(latestLocationIndex).latitude
                        val longitude = locationResult.locations.get(latestLocationIndex).longitude
                        viewModel.locationLiveData.value= LocationModel(latitude.toString(),longitude.toString())

                        setAddress(latitude, longitude)

                    }
                }


            }, Looper.getMainLooper()
        )
    }

    private fun setAddress(latitude: Double, longitude: Double) {
val geoCoder=Geocoder(activity, Locale.getDefault())
        try {
            val city=geoCoder.getFromLocation(latitude,longitude,1)[0].locality
            viewModel.getCurrentWeather(city )
        } catch (e: Exception) {
        }

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 1 && grantResults.isNotEmpty()) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getCurrentLocation()
            }
        } else {
            Toast.makeText(requireContext(), "Permission denied", Toast.LENGTH_SHORT).show()
        }
    }


}