package com.weatherApp.weatherforcast2.fragments

import android.graphics.Typeface
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer

import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.example.weatherforcast2.R

import com.example.weatherforcast2.databinding.FragmentSecondPageBinding

import com.weatherApp.weatherforcast2.adapters.Page2Adapter
import com.weatherApp.weatherforcast2.model.daily.DailyWeather
import com.weatherApp.weatherforcast2.viewmodel.NetworkViewModel


class SecondPageFragment : Fragment() {

    private lateinit var viewModel: NetworkViewModel

private lateinit var gridLayoutManager: GridLayoutManager
private lateinit var page2Adapter: Page2Adapter


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding: FragmentSecondPageBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_second_page, container, false)
        viewModel = ViewModelProvider(activity!!)[NetworkViewModel::class.java]
        setRecyclerView(binding)
        return binding.root
    }

    private fun  setRecyclerView(binding: FragmentSecondPageBinding) {
        val typeface = Typeface.createFromAsset(activity?.assets, "fonts/weather.ttf")
        gridLayoutManager= GridLayoutManager(activity,2)

        binding.recyclerViewPage2.layoutManager=gridLayoutManager

        viewModel.locationLiveData.observe(activity!!,{location->viewModel.getDailyWeather(location.lat,location.lon)})
            viewModel.dailyWeatherLiveData.observe(activity!!, object :Observer<DailyWeather>{
                override fun onChanged(t: DailyWeather?) {

                  if (t!=null){
                      page2Adapter= Page2Adapter(typeface,activity)
                      binding.recyclerViewPage2.adapter=page2Adapter}
                    page2Adapter.setData(t!!)

                }
            } )}

    }


