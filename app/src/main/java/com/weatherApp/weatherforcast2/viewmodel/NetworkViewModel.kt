package com.weatherApp.weatherforcast2.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope

import com.weatherApp.weatherforcast2.model.currnet.WeatherModel
import com.weatherApp.weatherforcast2.model.daily.Daily
import com.weatherApp.weatherforcast2.model.daily.DailyWeather
import com.weatherApp.weatherforcast2.model.latlon.LocationModel
import com.weatherApp.weatherforcast2.repository.Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response

class NetworkViewModel(application: Application) :AndroidViewModel(application) {
    private var repository: Repository = Repository()
    val weatherLiveData = object :MutableLiveData<WeatherModel>(){}
    val locationLiveData = object :MutableLiveData<LocationModel>(){}
    val dailyWeatherLiveData = object :MutableLiveData<DailyWeather>(){}
    val searchCurrentWeatherLiveData = object :MutableLiveData<WeatherModel>(){}
    val searchDailyWeatherLiveData = object :MutableLiveData<DailyWeather>(){}
    val searchDailyWeatherLiveData2 = object :MutableLiveData<Daily>(){}
    val check = object :MutableLiveData<Int>(){}

    fun getCurrentWeather(city:String) {

        viewModelScope.launch(Dispatchers.IO) {
            if (repository.getCurrentWeather(city).code() == 200) {
                weatherLiveData.postValue(repository.getCurrentWeather(city).body())
            }
        }
    }
    fun getDailyWeather(lat:String,lon:String) {
        viewModelScope.launch(Dispatchers.IO) {
            if (repository.getDaily7Weather(lat,lon).code() == 200) {
                dailyWeatherLiveData.postValue(repository.getDaily7Weather(lat,lon).body())
            }
        }
    }


    fun getSearchWeather(city:String) {
        viewModelScope.launch(Dispatchers.IO) {

            if (repository.getCurrentWeather(city).code() == 200) {
                searchCurrentWeatherLiveData.postValue(repository.getCurrentWeather(city).body())
                getSearchDailyWeather(repository.getCurrentWeather(city).body()!!.coord.lat.toString(),
                    repository.getCurrentWeather(city).body()!!.coord.lon.toString())

            }

        }
   }
    private  fun getSearchDailyWeather(lat:String,lon:String) {
        viewModelScope.launch(Dispatchers.IO) {
            if (repository.getDaily7Weather(lat,lon).code() == 200) {
                check.postValue(repository.getDaily7Weather(lat,lon).code())
                searchDailyWeatherLiveData.postValue(repository.getDaily7Weather(lat,lon).body())
            }
        }

    }



}