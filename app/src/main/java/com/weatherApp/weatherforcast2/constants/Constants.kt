package com.weatherApp.weatherforcast2.constants

class Constants {

    companion object{
        const val ClearSky="clear sky"
        const val FewClouds="few clouds"
        const val ScatteredClouds="scattered clouds"
        const val BrokenClouds="broken clouds"
        const val ShowerRain="shower rain"
        const val Rain="rain"
        const val ThunderStorm="thunderstorm"
        const val Snow="snow"
        const val Mist="mist"
const val Fog="fog"
        const val appid="a276177a147b658389e6a6df3f9bd470"


    }
}