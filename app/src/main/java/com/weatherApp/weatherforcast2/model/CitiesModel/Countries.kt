package com.weatherApp.weatherforcast2.model.CitiesModel

import com.google.gson.annotations.SerializedName

data class Countries (

	@SerializedName("States") val states : List<States>,
	@SerializedName("CountryName") val countryName : String
)