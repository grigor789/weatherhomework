package com.weatherApp.weatherforcast2.model.CitiesModel

import com.google.gson.annotations.SerializedName


data class States (

	@SerializedName("Cities") val cities : List<String>,
	@SerializedName("StateName") val stateName : String
)