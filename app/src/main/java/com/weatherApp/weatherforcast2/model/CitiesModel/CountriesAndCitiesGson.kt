
import com.google.gson.annotations.SerializedName
import com.weatherApp.weatherforcast2.model.CitiesModel.Countries


data class CountriesAndCitiesGson (

	@SerializedName("Countries") val countries : List<Countries>
)