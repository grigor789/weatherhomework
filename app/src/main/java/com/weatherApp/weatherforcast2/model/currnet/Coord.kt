package com.weatherApp.weatherforcast2.model.currnet

import com.google.gson.annotations.SerializedName


data class Coord (

	@SerializedName("lon") val lon : Double,
	@SerializedName("lat") val lat : Double
)