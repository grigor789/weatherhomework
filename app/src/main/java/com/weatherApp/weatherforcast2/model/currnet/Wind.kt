package com.weatherApp.weatherforcast2.model.currnet

import com.google.gson.annotations.SerializedName


data class Wind (

	@SerializedName("speed") val speed : Double,
	@SerializedName("deg") val deg : Int
)