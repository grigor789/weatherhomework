package com.weatherApp.weatherforcast2.model.currnet

import com.google.gson.annotations.SerializedName


data class Clouds (

	@SerializedName("all") val all : Int
)