package com.weatherApp.weatherforcast2.bindingAdapter

import androidx.appcompat.widget.AppCompatTextView
import androidx.databinding.BindingAdapter
import com.example.weatherforcast2.R

import com.weatherApp.weatherforcast2.constants.Constants
import java.util.*

@BindingAdapter(value = ["description","sunrise","sunset"],requireAll = false)
fun bindingIcon(view:AppCompatTextView,description:String?,sunrise:Long?,sunset:Long?){
    var currentTime= Date().time


    currentTime/=1000
    if (description!=null&&sunrise!=null&&sunset!=null){
    if (currentTime>= sunrise&&currentTime<=sunset){
        when(description){
            Constants.ClearSky->view.text=view.context.getString(R.string.clear_sky_day)
            Constants.FewClouds->view.text=view.context.getString(R.string.few_clouds_day)
            Constants.ScatteredClouds->view.text=view.context.getString(R.string.scattered_clouds_day)
            Constants.BrokenClouds->view.text=view.context.getString(R.string.broken_cloud_day)
            Constants.ShowerRain->view.text=view.context.getString(R.string.shower_rain)
            Constants.Rain->view.text=view.context.getString(R.string.rain_day)
            Constants.ThunderStorm->view.text=view.context.getString(R.string.thunderstorm)
            Constants.Snow->view.text=view.context.getString(R.string.snow)
            Constants.Mist->view.text=view.context.getString(R.string.mist)
            Constants.Fog->view.text=view.context.getString(R.string.mist)
        }
    }else{
        when(description){
            Constants.ClearSky->view.text=view.context.getString(R.string.clear_sky_night)
            Constants.FewClouds->view.text=view.context.getString(R.string.few_clouds_night)
            Constants.ScatteredClouds->view.text=view.context.getString(R.string.scattered_clouds_night)
            Constants.BrokenClouds->view.text=view.context.getString(R.string.broken_cloud_night)
            Constants.ShowerRain->view.text=view.context.getString(R.string.shower_rain)
            Constants.Rain->view.text=view.context.getString(R.string.rain_night)
            Constants.ThunderStorm->view.text=view.context.getString(R.string.thunderstorm)
            Constants.Snow->view.text=view.context.getString(R.string.snow)
            Constants.Mist->view.text=view.context.getString(R.string.mist)
            Constants.Fog->view.text=view.context.getString(R.string.mist)
    }
        if(description.contains("clouds")&&description!=Constants.BrokenClouds&&description!=Constants.FewClouds&&description!=Constants.ScatteredClouds){
            view.text=view.context.getString(R.string.broken_cloud_day)
        }
        if(description.contains("rain")&&description!=Constants.Rain&&description!=Constants.ShowerRain){
            view.text=view.context.getString(R.string.rain)
        }
        if(description.contains("snow")&&description!=Constants.Snow){
            view.text=view.context.getString(R.string.snowing)
}}}}