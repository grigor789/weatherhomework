package com.weatherApp.weatherforcast2.retrofitinstance


import com.weatherApp.weatherforcast2.api.WeatherApi
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object RetrofitInstance {
    private val interceptor= HttpLoggingInterceptor().apply { this.level=HttpLoggingInterceptor.Level.BASIC }

    private var okHttpClient: OkHttpClient =OkHttpClient.Builder().addInterceptor(interceptor).connectTimeout(30,TimeUnit.SECONDS).build()

    private val retrofit: Retrofit by lazy {Retrofit.Builder().addConverterFactory(GsonConverterFactory.create()).client(
        okHttpClient
    )
        .baseUrl("https://api.openweathermap.org/data/2.5/").build() }
    val api: WeatherApi by lazy { retrofit.create(WeatherApi::class.java) }
}