package com.weatherApp.weatherforcast2.adapters

import android.annotation.SuppressLint
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.ViewGroup

import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.example.weatherforcast2.R

import com.example.weatherforcast2.databinding.DailycardviewBinding
import com.weatherApp.weatherforcast2.constants.Constants
import com.weatherApp.weatherforcast2.model.daily.DailyWeather

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class Page2Adapter(private val typeface: Typeface,private val activity: FragmentActivity?) :
    RecyclerView.Adapter<Page2Adapter.Page2Holder>() {

    private var list= ArrayList<DailyWeather>()

    inner class Page2Holder(val binding: DailycardviewBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Page2Holder {
        val binding: DailycardviewBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.dailycardview, parent, false
        )
        return Page2Holder(binding)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: Page2Holder, position: Int) {
        holder.binding.iconW.typeface = typeface
        holder.binding.tempDay.also {it.typeface=typeface
            it.text =
                activity!!.getString(R.string.sun) + " " + list[0].daily[position].temp.day+" °C"
        }
        holder.binding.tempNight.also {
            it.typeface = typeface
            it.text = activity!!.getString(R.string.moon) + " " + list[0].daily[position].temp.night+" °C"
        }
        holder.binding.humidity.text="Humidity "+list[0].daily[position].humidity.toString()+" %"
        holder.binding.pressure.text="Pressure "+list[0].daily[position].pressure.toString()+" hPa"
        holder.binding.cardDescription.text=list[0].daily[position].weather[0].main


        setIcon(position,holder)

        setDate(position,holder)

    }




    override fun getItemCount(): Int {
if (list.isNotEmpty()){
    return list[0].daily.size
}
     return 0 }



    fun setData(dailyWeather: DailyWeather){
        list.add(dailyWeather)
        notifyDataSetChanged()
    }
    private fun setIcon(
        position: Int,
        holder: Page2Holder
    ) {val view=holder.binding.iconW
        val description=list[0].daily[position].weather[0].description

        when (description) {
                Constants.ClearSky -> view.text = view.context.getString(R.string.clear_sky_day)
                Constants.FewClouds -> view.text = view.context.getString(R.string.few_clouds_day)
                Constants.ScatteredClouds -> view.text =
                    view.context.getString(R.string.scattered_clouds_day)
                Constants.BrokenClouds -> view.text =
                    view.context.getString(R.string.broken_cloud_day)
                Constants.ShowerRain -> view.text = view.context.getString(R.string.shower_rain)
                Constants.Rain -> view.text = view.context.getString(R.string.rain_day)
                Constants.ThunderStorm -> view.text = view.context.getString(R.string.thunderstorm)
                Constants.Snow -> view.text = view.context.getString(R.string.snow)
                Constants.Mist -> view.text = view.context.getString(R.string.mist)
            }

            if(description.contains("clouds")&&description!=Constants.BrokenClouds&&description!=Constants.FewClouds&&description!=Constants.ScatteredClouds){
                view.text=view.context.getString(R.string.broken_cloud_day)
            }
            if(description.contains("rain")&&description!=Constants.Rain&&description!=Constants.ShowerRain){
                view.text=view.context.getString(R.string.rain)
            }
            if(description.contains("snow")&&description!=Constants.Snow){
                view.text=view.context.getString(R.string.snowing)
        }
    }
   @SuppressLint("SimpleDateFormat")
   private fun setDate(position: Int, holder: Page2Holder){
        val time:Long=list[0].daily[position].dt.toLong()*1000
       holder.binding.cardDate.text=SimpleDateFormat("dd/MM/yyyy").format(time)

    }}