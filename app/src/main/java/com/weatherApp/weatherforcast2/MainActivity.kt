package com.weatherApp.weatherforcast2


import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil

import com.example.weatherforcast2.databinding.ActivityMainBinding

import com.weatherApp.weatherforcast2.adapters.ViewPagerAdapter
import com.weatherApp.weatherforcast2.api.ConnectionCheck
import com.weatherApp.weatherforcast2.fragments.FirstPageFragment
import com.weatherApp.weatherforcast2.fragments.NoInternetFragment
import com.weatherApp.weatherforcast2.fragments.SearchFragment
import com.weatherApp.weatherforcast2.fragments.SecondPageFragment

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,
            _root_ide_package_.com.example.weatherforcast2.R.layout.activity_main
        )
        val connectionCheck = ConnectionCheck(applicationContext)
        connectionCheck.observe(this,
            { t ->
                if (t!!) {
                    setUpTabs()
                } else {
                    removeTabs()
                }
            })


    }

    private fun setUpTabs() {
        binding.tabs.visibility = View.VISIBLE
        binding.containerStart.visibility = View.GONE
        binding.viewPager.visibility = View.VISIBLE

        val adapter = ViewPagerAdapter(supportFragmentManager)

        adapter.addFragment(FirstPageFragment(), "Current")
        adapter.addFragment(SecondPageFragment(), "Daily")
        adapter.addFragment(SearchFragment(), "Search")
        binding.viewPager.adapter = adapter
        binding.tabs.setupWithViewPager(binding.viewPager)
    }

    private fun removeTabs() {
        binding.tabs.visibility = View.GONE
        binding.containerStart.visibility = View.VISIBLE
        binding.viewPager.visibility = View.GONE
        supportFragmentManager.beginTransaction().add(_root_ide_package_.com.example.weatherforcast2.R.id.containerStart, NoInternetFragment())
            .commit()
    }
}