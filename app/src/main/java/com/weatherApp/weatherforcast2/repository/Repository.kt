package com.weatherApp.weatherforcast2.repository

import com.weatherApp.weatherforcast2.constants.Constants
import com.weatherApp.weatherforcast2.model.currnet.WeatherModel
import com.weatherApp.weatherforcast2.model.daily.DailyWeather
import com.weatherApp.weatherforcast2.retrofitinstance.RetrofitInstance
import retrofit2.Response

class Repository {
    suspend fun getCurrentWeather(city:String):Response<WeatherModel>
    = RetrofitInstance.api.getCurrentWeather(Constants.appid,city,"metric")
    suspend fun getDaily7Weather(lat:String,lon:String)
    :Response<DailyWeather> =RetrofitInstance.api.getDaily7days(Constants.appid,lat, lon,"metric","hourly")
}